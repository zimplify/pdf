<?php
    namespace Zimplify\PDF\Providers;
    use Zimplify\Core\{Application, File, Instance, Provider};
    use Zimplify\Core\Services\{DataUtils};
    use \RuntimeException;
    
    /**
     * <provide description for this service provider>
     * @package Zimplify\PDF (code 09)
     * @type Provider (code 03)
     * @file PdfProvider (code 01)
     */
    class PdfProvider extends Provider {
    
        const DEF_CLS_NAME = "Zimplify\PDF\\Providers\\PdfProvider";
        const DEF_SHT_NAME = "core-pdf::pdf";
        const ERR_NO_SOURCE = 404090301001;
    
        /**
         * generate the contents in PDF syntax and ready for storage
         * @param string $contents the content to generate into PDF
         * @return string
         */
        protected function generate(string $contents) : string {

        }

        /**
         * the rendering of the PDF file in HTML structure
         * @param string $template template file for rendering
         * @param Instance $source the data source for the render
         * @return File
         */
        public function render(string $template, Instance $source) : File {
            $file = explode("::", $template);
            if (count($file) == 2) {
                $target = Application::fetch($file[1], self::DEF_TEMPLATE_TYPE, $file[0]);
                $contents = $template;

                // now rendering the PDF contents
                $tags = preg_match_all("/\{\{*\}\}/", $template);

                // only do things when we have to...                
                if (count($tags) > 0 && count($tags[0]) > 0) {
                    foreach ($tags[0] ?? [] as $tag) {
                        $request = substr($tag, 2, strlen($tag) - 4);
                        $value = DataUtils::evaluate($request, $source, []);
                        $contents = str_replace($tag, $value, $contents);
                    }                    
                }

                // now generate the PDF file
                $output = $this->generate($contents);

                // store the file to S3
                
                // sending out the file
                $result = File();
                $result->mime = "application/pdf";
                return $result;
            } else  
                throw new RuntimeException("Unable to locate template for PDF render", self::ERR_NO_SOURCE);
            
        }
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }
    }